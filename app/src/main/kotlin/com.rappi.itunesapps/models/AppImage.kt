package com.rappi.itunesapps.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * App Image Url
 * @author Gabriel Rodriguez
 * @version 1.0
 */
class AppImage : Serializable {

    @SerializedName("label") lateinit var url: String
}