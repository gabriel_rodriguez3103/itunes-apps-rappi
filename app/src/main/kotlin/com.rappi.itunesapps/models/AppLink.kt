package com.rappi.itunesapps.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * @author Gabriel Rodriguez
 * @version 1.0
 */
class AppLink : Serializable {

    @SerializedName("href") lateinit var url: String
}