package com.rappi.itunesapps.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

/**
 * App Category Model
 * @author Gabriel Rodriguez
 * @version 1.0
 */
class AppCategory : Serializable {

    @SerializedName("im:id") lateinit var id: String
    @SerializedName("label") lateinit var name: String

}