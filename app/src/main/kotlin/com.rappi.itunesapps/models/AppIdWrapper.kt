package com.rappi.itunesapps.models

import java.io.Serializable

/**
 * @author Gabriel Rodriguez
 * @version 1.0
 */
class AppIdWrapper : Serializable {

    lateinit var attributes: AppId
}