package com.rappi.itunesapps.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Response from web service
 * @author Gabriel Rodriguez
 * @version 1.0
 */
open class Response : Serializable {

    @SerializedName("feed") lateinit var response: AppsWrapper
}