package com.rappi.itunesapps.models

import java.util.*

/**
 * Web service response wrapper
 * @author Gabriel Rodriguez
 * @version 1.0
 */
class ResponseWrapper : Response() {
}