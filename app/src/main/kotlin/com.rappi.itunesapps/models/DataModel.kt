package com.rappi.itunesapps.models

import io.realm.Realm
import io.realm.RealmList
import java.util.*
import kotlin.properties.Delegates

/**
 * Data Model Save an loaded from the data base
 * @author Gabriel Rodriguez
 * @version 1.0
 */
object DataModel {

    var categories = RealmList<CategoryModel>()
    var realm: Realm by Delegates.notNull()

    fun setProductByCategory(apps: ArrayList<App>) {
        for (category in categories) {
            category.getAppsByCategory(apps)
        }
    }

    init {
        categories.clear()
        realm = Realm.getDefaultInstance()
        val result = realm.where(CategoryModel::class.java).findAll()
        categories.addAll(result.subList(0, result.size))
    }

    fun saveCategories() {
        realm.executeTransactionAsync ({ realm ->
            for (category in categories) {
                realm.copyToRealmOrUpdate(category)
            }
        }, {
            //success
        }, { error ->
            //error
        })
    }

}