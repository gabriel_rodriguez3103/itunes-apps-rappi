package com.rappi.itunesapps.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * App Summary
 * @author Gabriel Rodriguez
 * @version 1.0
 */
class AppSummary : Serializable {

    @SerializedName("label") lateinit var summary: String
}