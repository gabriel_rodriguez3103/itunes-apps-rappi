package com.rappi.itunesapps.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import java.io.Serializable
import java.util.*

/**
 * App Response Wrapper
 * @author Gabriel Rodriguez
 * @version 1.0
 */
class AppsWrapper : Serializable {

    @SerializedName("entry") lateinit var apps: ArrayList<App>

    /**
     * Get the Categories from an [ArrayList] [App]
     * @return categories
     */
    fun getCategories(): RealmList<CategoryModel> {
        val categories = RealmList<CategoryModel>()
        for (app in apps) {
            val category = CategoryModel(app.category.attributes)
            if (!categories.contains(category)) categories.add(category)
        }
        return categories
    }
}