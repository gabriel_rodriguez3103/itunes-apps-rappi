package com.rappi.itunesapps.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

/**
 * App Model
 * @author Gabriel Rodriguez
 * @version 1.0
 */
class App : Serializable {

    lateinit var id: AppIdWrapper
    @SerializedName("im:name") lateinit var name: AppName
    @SerializedName("im:image") lateinit var image: ArrayList<AppImage>
    lateinit var summary: AppSummary
    lateinit var category: AppCategoryWrapper
    lateinit var rights: AppRights
    lateinit var link: AppLinkWrapper

}