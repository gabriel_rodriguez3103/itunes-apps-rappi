package com.rappi.itunesapps.models

/**
 * App Category Info Wrapper
 * @author Gabriel Rodriguez
 * @version 1.0
 */
class AppCategoryWrapper {

    lateinit var attributes: AppCategory

}