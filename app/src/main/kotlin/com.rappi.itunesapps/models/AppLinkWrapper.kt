package com.rappi.itunesapps.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * @author Gabriel Rodriguez
 * @version 1.0
 */
class AppLinkWrapper : Serializable {

    lateinit var attributes: AppLink
}