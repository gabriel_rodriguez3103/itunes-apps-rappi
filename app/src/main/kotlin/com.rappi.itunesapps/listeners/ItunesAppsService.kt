package com.rappi.itunesapps.listeners

import com.rappi.itunesapps.models.ResponseWrapper
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * @author Gabriel Rodriguez
 * @version 1.0
 */
interface ItunesAppsService {

    companion object {
        val SERVICE_ENDPOINT = "https://itunes.apple.com"
    }

    /**
     * Get the top List of Itunes Apps
     * @return Wrapper with web service response
     */
    @GET("/us/rss/topfreeapplications/{limit}/json")
    fun getApps(@Path("limit") limit: String): Call<ResponseWrapper>
}