package com.rappi.itunesapps.listeners

import com.rappi.itunesapps.models.AppsWrapper

/**
 * Callback to notify about the web service result
 * @author Gabriel Rodriguez
 */
interface OnServiceResult {

    /**
     * Called after web service result
     * @param apps returned from the web service
     */
    fun onResult(apps: AppsWrapper?)
}