package com.rappi.itunesapps.service

import com.rappi.itunesapps.R
import com.rappi.itunesapps.RappiApplication
import com.rappi.itunesapps.listeners.ItunesAppsService
import com.rappi.itunesapps.listeners.OnServiceResult
import com.rappi.itunesapps.models.ResponseWrapper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Object to manage web service request
 * @author Gabriel Rodriguez.
 * @version 1.0
 */

object RequestDispatcher {

    val service by lazy { RappiApplication.getRetrofit().create(ItunesAppsService::class.java) }
    val resources by lazy { RappiApplication.getAppInstance().resources }

    fun getApps(callback: OnServiceResult) {
        service.getApps(resources.getString(R.string.limit_path, "20")).enqueue(object : Callback<ResponseWrapper> {
            override fun onResponse(call: Call<ResponseWrapper>?, response: Response<ResponseWrapper>?) {
                if (response?.isSuccessful ?: false) callback.onResult(response!!.body().response)
                else callback.onResult(null)
            }
            override fun onFailure(call: Call<ResponseWrapper>?, t: Throwable?) {
                callback.onResult(null)
            }

        })
    }

}
