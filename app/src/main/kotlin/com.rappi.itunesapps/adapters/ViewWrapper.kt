package com.repos.adapter

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Copied from his owner.
 * @author Samuel Barbosa
 * @version 1.0
 */
class ViewWrapper<V : View>(val view: V) : RecyclerView.ViewHolder(view) {
}