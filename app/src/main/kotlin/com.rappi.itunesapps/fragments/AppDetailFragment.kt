package com.rappi.itunesapps.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rappi.itunesapps.R
import com.rappi.itunesapps.RappiApplication
import com.rappi.itunesapps.models.AppModel
import com.rappi.itunesapps.models.DataModel
import kotlinx.android.synthetic.main.app_detail_header.*
import kotlinx.android.synthetic.main.fragment_app_detail.*

/**
 * @author Gabriel Rodriguez
 * @version 1.0
 */

class AppDetailFragment : Fragment() {

    lateinit var app: AppModel
    val phoneConfiguration by lazy { resources.getBoolean(R.bool.phone_configuration) }

    companion object {
        val APP_DETAIL_FRAGMENT_TAG = "appDetailFragmentTag"
        val APP_POSITION = "appPosition"
        val CATEGORY_POSITION = "categoryPosition"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        retainInstance = true
        return inflater?.inflate(R.layout.fragment_app_detail, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        app = DataModel.categories[arguments.getInt(CATEGORY_POSITION)].apps[arguments.getInt(APP_POSITION)]
        if (phoneConfiguration) setupToolbar()
        activity.runOnUiThread { bind() }

    }

    fun setupToolbar() {
        detail_toolbar.setNavigationIcon(R.drawable.ic_back)
        detail_toolbar.setNavigationOnClickListener { activity.onBackPressed() }
    }

    fun bind() {
        RappiApplication.getGlideInstace().load(app.image).centerCrop().into(image)
        name.text = app.rights
        collapsing.title = app.name
        app_summary.text = app.summary
        if (!app.name.contains(' '))
            link.text = resources.getString(R.string.check_it_message, app.name)
        else
            link.text = resources.getString(R.string.check_it_message, app.name.substring(0, app.name.indexOf(' ')))
        link.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(app.link)))
        }
    }
}
