package com.rappi.itunesapps.models;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Category Model Save an loaded from the data base
 *
 * @author Gabriel Rodriguez
 * @version 1.0
 */
public class AppModel extends RealmObject implements Serializable {

    @PrimaryKey
    private String id;
    private String name;
    private String summary;
    private String image;
    private String rights;
    private String link;

    public AppModel() {
    }

    public AppModel(App app) {
        this.id = app.getId().getAttributes().getId();
        this.name = app.getName().getName();
        this.summary = app.getSummary().getSummary();
        this.image = app.getImage().get(2).getUrl();
        this.rights = app.getRights().getName();
        this.link = app.getLink().getAttributes().getUrl();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRights() {
        return rights;
    }

    public void setRights(String rights) {
        this.rights = rights;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        return (other instanceof AppModel && ((AppModel) other).id.equals(this.id));
    }
}
