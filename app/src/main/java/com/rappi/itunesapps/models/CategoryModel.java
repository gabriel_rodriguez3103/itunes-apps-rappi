package com.rappi.itunesapps.models;

import java.io.Serializable;
import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Category Model Save an loaded from the data base
 *
 * @author Gabriel Rodriguez
 * @version 1.0
 */
public class CategoryModel extends RealmObject implements Serializable {

    @PrimaryKey
    private String id;
    private String name;
    private RealmList<AppModel> apps = new RealmList<>();

    public CategoryModel() {
    }

    public CategoryModel(AppCategory category) {
        this.id = category.getId();
        this.name = category.getName();
    }

    public void getAppsByCategory(ArrayList<App> apps) {
        for (App app : apps) {
            if (this.equals(new CategoryModel(app.getCategory().getAttributes())))
                this.apps.add(new AppModel(app));
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<AppModel> getApps() {
        return apps;
    }

    public void setApps(RealmList<AppModel> apps) {
        this.apps = apps;
    }

    @Override
    public boolean equals(Object other) {
        return (other instanceof CategoryModel && ((CategoryModel) other).id.equals(this.id));
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
