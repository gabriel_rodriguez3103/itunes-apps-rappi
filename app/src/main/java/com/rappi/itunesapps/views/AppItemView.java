package com.rappi.itunesapps.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rappi.itunesapps.R;
import com.rappi.itunesapps.RappiApplication;
import com.rappi.itunesapps.models.App;
import com.rappi.itunesapps.models.AppModel;

/**
 * @author Gabriel Rodriguez
 * @version 1.0
 */
public class AppItemView extends RelativeLayout {

    public AppItemView(Context context) {
        super(context);
        init(context);
    }

    public AppItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AppItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.app_item_view, this, true);
    }

    public void bind(AppModel app) {
        RappiApplication.getGlideInstace().load(app.getImage()).centerCrop().into((ImageView) findViewById(R.id.app_image));
        ((TextView) findViewById(R.id.app_name)).setText(app.getName());
    }
}
