package com.rappi.itunesapps.listeners;

/**
 * @author Gabriel Rodriguez
 * @version 1.0
 */
public interface OnAppClickEvent {

    void onItemClick(int categoryPosition, int appPosition);
}
