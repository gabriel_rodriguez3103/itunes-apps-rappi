package com.rappi.itunesapps;

import android.app.Application;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.rappi.itunesapps.listeners.ItunesAppsService;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Application context to instance different usefull classes
 *
 * @author Gabriel Rodriguez
 * @version 1.0
 */
public class RappiApplication extends Application {

    private static RappiApplication instance;
    private static RequestManager glide;
    private static Retrofit retrofit;
    public final static String FRAGMENT_BACK_STACK = "AppBackStack";

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        // Calligraphy Initialize
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().setDefaultFontPath(getString(R.string.font_regular))
                .setFontAttrId(R.attr.fontPath).build());
        // Init Glide
        glide = Glide.with(this);
        retrofit = new Retrofit.Builder()
                .baseUrl(ItunesAppsService.Companion.getSERVICE_ENDPOINT())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        // Realm
        Realm.setDefaultConfiguration(new RealmConfiguration.Builder(this)
                .deleteRealmIfMigrationNeeded()
                .build());
    }

    public static RappiApplication getAppInstance() {
        return instance;
    }

    public static RequestManager getGlideInstace() {
        return glide;
    }

    public static Retrofit getRetrofit() {
        return retrofit;
    }
}
