package com.rappi.itunesapps.adapters;

import android.view.ViewGroup;

import com.rappi.itunesapps.models.App;
import com.rappi.itunesapps.models.AppCategory;
import com.rappi.itunesapps.models.AppModel;
import com.rappi.itunesapps.views.AppItemView;
import com.repos.adapter.RecyclerViewBaseAdapter;
import com.repos.adapter.ViewWrapper;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * Adapter to Show the apps
 * @author Gabriel Rodriguez
 * @version 1.0
 */
public class AppsAdapter extends RecyclerViewBaseAdapter<AppModel, AppItemView> {

    @NotNull
    @Override
    protected AppItemView onCreateItemView(@NotNull ViewGroup parent, int viewType) {
        return new AppItemView(parent.getContext());
    }

    @Override
    public void onBindViewHolder(ViewWrapper<AppItemView> holder, int position) {
        holder.getView().bind(getItems().get(position));
    }
}
