package com.rappi.itunesapps.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.rappi.itunesapps.fragments.AppsFragment;
import com.rappi.itunesapps.models.DataModel;

/**
 * View Pager Adapter to show all available categories
 *
 * @author Gabriel Rodriguez
 * @version 1.0
 */
public class CategoriesAdapter extends FragmentStatePagerAdapter {

    public CategoriesAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return AppsFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return DataModel.INSTANCE.getCategories().size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return DataModel.INSTANCE.getCategories().get(position).getName();
    }
}
