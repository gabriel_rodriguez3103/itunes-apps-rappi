package com.rappi.itunesapps.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rappi.itunesapps.R;
import com.rappi.itunesapps.adapters.AppsAdapter;
import com.rappi.itunesapps.listeners.OnAppClickEvent;
import com.rappi.itunesapps.models.CategoryModel;
import com.rappi.itunesapps.models.DataModel;
import com.repos.adapter.SpacesItemDecoration;
import com.rohit.recycleritemclicksupport.RecyclerItemClickSupport;

/**
 * Fragment to Show the Apps divide by categories
 *
 * @author Gabriel Rodriguez
 * @version 1.0
 */
public class AppsFragment extends Fragment {

    private static final String CATEGORY_POSITION = "categoryPosition";
    private View rootView;
    private CategoryModel category;
    private AppsAdapter mAppsAdapter = new AppsAdapter();
    OnAppClickEvent mCallback;

    public AppsFragment() {
    }

    /**
     * Fragment instance
     *
     * @param position of category to inflate
     * @return AppFragment instance
     */
    public static AppsFragment newInstance(int position) {
        AppsFragment fragment = new AppsFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(CATEGORY_POSITION, position);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (OnAppClickEvent) context;
        } catch (Exception e) {
            throw new ClassCastException(getString(R.string.class_exception_message,
                    context.toString(), OnAppClickEvent.class.getSimpleName()));
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_apps, container, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rootView = view;
        category = DataModel.INSTANCE.getCategories().get(getArguments().getInt(CATEGORY_POSITION));
        setupRecyclerView();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                bind();
            }
        });
    }

    private void setupRecyclerView() {
        RecyclerView appsRecyclerView = (RecyclerView) rootView.findViewById(R.id.rv_apps);
        appsRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(
                getContext().getResources().getInteger(R.integer.number_of_items), StaggeredGridLayoutManager.VERTICAL));
        appsRecyclerView.setHasFixedSize(true);
        appsRecyclerView.addItemDecoration(new SpacesItemDecoration((int) getResources().getDimension(R.dimen.medium_space)));
        appsRecyclerView.setAdapter(mAppsAdapter);
        RecyclerItemClickSupport.addTo(appsRecyclerView).setOnItemClickListener(new RecyclerItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                mCallback.onItemClick(getArguments().getInt(CATEGORY_POSITION), position);
            }
        });
    }

    /**
     * Bind the apps on the the recyclerView
     */
    private void bind() {
        mAppsAdapter.setItems(category.getApps());
    }
}
