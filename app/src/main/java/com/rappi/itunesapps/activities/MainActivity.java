package com.rappi.itunesapps.activities;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.rappi.itunesapps.R;
import com.rappi.itunesapps.RappiApplication;
import com.rappi.itunesapps.adapters.CategoriesAdapter;
import com.rappi.itunesapps.fragments.AppDetailFragment;
import com.rappi.itunesapps.listeners.OnAppClickEvent;
import com.rappi.itunesapps.listeners.OnServiceResult;
import com.rappi.itunesapps.models.AppsWrapper;
import com.rappi.itunesapps.models.CategoryModel;
import com.rappi.itunesapps.models.DataModel;
import com.rappi.itunesapps.service.RequestDispatcher;
import com.rappi.itunesapps.utils.Utility;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

import butterknife.BindBool;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements OnServiceResult, OnAppClickEvent {

    @BindView(R.id.toolbar)
    Toolbar mainToolbar;
    @BindView(R.id.apps_viewpager)
    ViewPager viewPager;
    @BindView(R.id.category_tab)
    TabLayout categoriesTab;
    @BindView(R.id.drawer)
    DrawerLayout drawer;
    @BindView(R.id.navigation_view)
    NavigationView navigationView;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindBool(R.bool.phone_configuration)
    boolean phoneConfiguration;

    CategoriesAdapter categoriesAdapter;
    ArrayList<MenuItem> categories = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupMainToolbar();
        setupViewPager();
        setupTabLayout();
        setupNavigationView();
        setupSwipeToRefresh();
        loadData();
    }

    private void setupMainToolbar() {
        mainToolbar.setNavigationIcon(R.drawable.ic_menu);
        mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(navigationView);
            }
        });
    }

    /**
     * Load data from the Api if the data base is empty
     */
    private void loadData() {
        if (DataModel.INSTANCE.getCategories().size() > 0) inflateDataOnViews();
        else getApps();
    }

    /**
     * Get apps from the api web service
     */
    private void getApps() {
        RequestDispatcher.INSTANCE.getApps(this);
    }

    private void setupViewPager() {
        categoriesAdapter = new CategoriesAdapter(getSupportFragmentManager());
        viewPager.setAdapter(categoriesAdapter);
    }

    private void setupTabLayout() {
        categoriesTab.setupWithViewPager(viewPager);
    }

    /**
     * Update the views after get the data from the web service or data base
     */
    private void inflateDataOnViews() {
        categoriesAdapter.notifyDataSetChanged();
        categories.clear();
        navigationView.getMenu().clear();
        int index = 0;
        for (CategoryModel category : DataModel.INSTANCE.getCategories()) {
            navigationView.getMenu().add(R.id.categories, category.hashCode(), Menu.NONE, category.getName());
            categories.add(navigationView.getMenu().getItem(index));
            index++;
        }
        if (!phoneConfiguration) {
            populateFragmentContainer(0, 0);
        }
    }

    private void setupNavigationView() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                viewPager.setCurrentItem(categories.indexOf(item));
                drawer.closeDrawers();
                return true;
            }
        });
    }

    private void populateFragmentContainer(int categoryPosition, int appPosition) {
        getSupportFragmentManager().popBackStackImmediate(RappiApplication.FRAGMENT_BACK_STACK,
                FragmentManager.POP_BACK_STACK_INCLUSIVE);
        AppDetailFragment appDetailFragment = new AppDetailFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(AppDetailFragment.Companion.getAPP_POSITION(), appPosition);
        arguments.putInt(AppDetailFragment.Companion.getCATEGORY_POSITION(), categoryPosition);
        appDetailFragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right)
                .add(R.id.fragment_container, appDetailFragment, AppDetailFragment.Companion.getAPP_DETAIL_FRAGMENT_TAG())
                .addToBackStack(RappiApplication.FRAGMENT_BACK_STACK).commit();
    }

    private void setupSwipeToRefresh() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getApps();
            }
        });
    }

    private void showNoConectivitySnackBar(@StringRes int resId) {
        Snackbar.make(coordinatorLayout, resId, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getApps();
                    }
                }).show();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }
        if (!phoneConfiguration) {
            finish();
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void onResult(@Nullable AppsWrapper apps) {
        swipeRefreshLayout.setRefreshing(false);
        if (apps != null) {
            DataModel.INSTANCE.setCategories(apps.getCategories());
            DataModel.INSTANCE.setProductByCategory(apps.getApps());
            DataModel.INSTANCE.saveCategories();
            inflateDataOnViews();
            return;
        }
        if (DataModel.INSTANCE.getCategories().size() == 0 && !Utility.isNetworkAvailable(this))
            showNoConectivitySnackBar(R.string.internet_error);
        else showNoConectivitySnackBar(R.string.update_error);
    }

    @Override
    public void onItemClick(int categoryPosition, int appPosition) {
        populateFragmentContainer(categoryPosition, appPosition);
    }
}